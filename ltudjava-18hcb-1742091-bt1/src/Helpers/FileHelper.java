/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import models.enumObj;

/**
 *
 * @author HoiNT2
 */
public class FileHelper {
    /**
     * write file
     *
     * @param <T>
     * @param lst
     * @param fileName
     * @param title:
     * @return
     */
    public static <T> boolean writeFile(ArrayList<T> lst, String fileName, String title) throws IOException {
        BufferedWriter bufferedWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file, false), "UTF8"));
            System.out.println(fileName);
            bufferedWriter.write(title);
            bufferedWriter.newLine();
            for (T item : lst) {
                bufferedWriter.append(item.toString());
                bufferedWriter.newLine();
                System.out.println(item.toString());
            }
            bufferedWriter.flush();
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        } finally {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
        }
    }

    public static <T> ArrayList<T> readFileNew(Class<T> tClass, String filename, enumObj eObj) throws IOException {
        File file = null;
        BufferedReader bufferedReader = null;
        if (filename.isEmpty()) {
            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            FileNameExtensionFilter filter = new FileNameExtensionFilter(
                    "CSV files (*csv)", "csv");
            jfc.setFileFilter(filter);
            int returnValue = jfc.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                filename = jfc.getSelectedFile().getName();
                if (!getFileExtension(filename).equalsIgnoreCase(".csv")) {
                    return null;
                }
                file = jfc.getSelectedFile();
            }
        } else {
            file = new File(filename);
            if (!file.exists()) {
                return null;
            }
        }
        ArrayList<T> list = new ArrayList<>();
        try {

            bufferedReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(file), "UTF-8"));
            String readLine = "";
            String title = bufferedReader.readLine(); //  read title       
            while ((readLine = bufferedReader.readLine()) != null) {
                if (!readLine.isEmpty() && readLine.contains(",")) {
                    String[] arr = readLine.split(",");
                    if (tClass.getFields().length == arr.length) {
                        T t = (T) enumObj.getEnum(eObj, arr);
                        if (t != null) {
                            list.add(t);
                        }
                    }

                }
            }
            return list;
        } catch (IOException | ClassCastException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
    }

    private static String getFileExtension(String fileName) {
        String extension = "";
        try {
            if (!fileName.isEmpty()) {
                extension = fileName.substring(fileName.lastIndexOf("."));
            }
        } catch (Exception e) {
            extension = "";
        }
        return extension;
    }
}
