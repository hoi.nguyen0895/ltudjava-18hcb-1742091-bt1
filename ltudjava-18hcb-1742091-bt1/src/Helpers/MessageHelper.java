/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author HoiNT2
 */
public class MessageHelper {

    final static String INFO = "Thông Báo";

    public static void Message(String content, String subject, int icon) {
        JOptionPane.showMessageDialog(new JFrame(), content, subject, icon);
    }

    public static void Message(String content) {
        JOptionPane.showMessageDialog(new JFrame(), content);
    }

    public static void MessageError(String content) {
        Message(content, INFO, JOptionPane.ERROR_MESSAGE);
    }

    public static boolean showConfirmDialog(String content) {
        int dialogResult = JOptionPane.showConfirmDialog(null, content, INFO, JOptionPane.YES_NO_OPTION);
        return dialogResult == JOptionPane.YES_OPTION;
    }
    public static int showConfirmDialogCancel(String content) {
        return  JOptionPane.showConfirmDialog(null, content, INFO, JOptionPane.YES_NO_CANCEL_OPTION);
    }
}
