/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author HoiNT2
 */
public class accountObj {

    public int Id;
    public String userName;
    public String passWord;
    public int groupId;
    public String Code;

    public accountObj() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public accountObj(int Id, String userName, String passWord, int groupId, String Code) {
        this.Id = Id;
        this.userName = userName;
        this.passWord = passWord;
        this.groupId = groupId;
        this.Code = Code;
    }

    @Override
    public String toString() {
        return Id + "," + userName + "," + passWord + "," + groupId + "," + Code;
    }

}
