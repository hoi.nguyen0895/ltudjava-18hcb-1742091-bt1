/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author HoiNT2
 */
public class studentObj{

    public Integer STT;
    public String MSSV;
    public String HoTen;
    public String GioiTinh;
    public String CMND;

    public studentObj() {
    }

    public studentObj(String str) {
        String[] arr = str.split(",");
        //return new studentObj(Integer.parseInt(str[0]),str[1],str[2],str[3],str[4]);
        this.STT = Integer.parseInt(arr[0]);
        this.MSSV = arr[1];
        this.HoTen = arr[2];
        this.GioiTinh = arr[3];
        this.CMND = arr[4];
    }
    public studentObj (String arr[]) {
       this.STT = Integer.parseInt(arr[0]);
        this.MSSV = arr[1];
        this.HoTen = arr[2];
        this.GioiTinh = arr[3];
        this.CMND = arr[4];
    }
    public studentObj(Integer STT, String MSSV, String HoTen, String GioiTinh, String CMND) {
        this.STT = STT;
        this.MSSV = MSSV;
        this.HoTen = HoTen;
        this.GioiTinh = GioiTinh;
        this.CMND = CMND;
    }

    public Integer getSTT() {
        return STT;
    }

    public void setSTT(Integer STT) {
        this.STT = STT;
    }

    public String getMSSV() {
        return MSSV;
    }

    public void setMSSV(String MSSV) {
        this.MSSV = MSSV;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public String getGioiTinh() {
        return GioiTinh;
    }

    public void setGioiTinh(String GioiTinh) {
        this.GioiTinh = GioiTinh;
    }

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

    @Override
    public String toString() {
        return STT + "," + MSSV + "," + HoTen + "," + GioiTinh + "," + CMND;
    }

}
