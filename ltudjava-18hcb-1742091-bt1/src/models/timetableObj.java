/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author HoiNT2
 */
public class timetableObj {

    public int Id;
    public String Code;
    public String Name;
    public String Classroom;

    public timetableObj() {
    }

    public timetableObj(int Id, String Code, String Name, String Classroom) {
        this.Id = Id;
        this.Code = Code;
        this.Name = Name;
        this.Classroom = Classroom;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getClassroom() {
        return Classroom;
    }

    public void setClassroom(String Classroom) {
        this.Classroom = Classroom;
    }

    @Override
    public String toString() {
        return Id + "," + Code + "," + Name + "," + Classroom;
    }
}
