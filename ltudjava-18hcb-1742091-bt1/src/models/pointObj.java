/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author HoiNT2
 */
public class pointObj {

    public int Id;
    public String Code;
    public String StudentName;
    public float MidtermPoint;
    public float EndPoint;
    public float OtherPoint;
    public float FinalGrade;

    public pointObj() {
    }

    public pointObj(int Id, String Code, String StudentName, float MidtermPoint, float EndPoint, float OtherPoint, float FinalGrade) {
        this.Id = Id;
        this.Code = Code;
        this.StudentName = StudentName;
        this.MidtermPoint = MidtermPoint;
        this.EndPoint = EndPoint;
        this.OtherPoint = OtherPoint;
        this.FinalGrade = FinalGrade;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public float getMidtermPoint() {
        return MidtermPoint;
    }

    public void setMidtermPoint(float MidtermPoint) {
        this.MidtermPoint = MidtermPoint;
    }

    public float getEndPoint() {
        return EndPoint;
    }

    public void setEndPoint(float EndPoint) {
        this.EndPoint = EndPoint;
    }

    public float getOtherPoint() {
        return OtherPoint;
    }

    public void setOtherPoint(float OtherPoint) {
        this.OtherPoint = OtherPoint;
    }

    public float getFinalGrade() {
        return FinalGrade;
    }

    public void setFinalGrade(float FinalGrade) {
        this.FinalGrade = FinalGrade;
    }

    @Override
    public String toString() {
        return  Id + "," + Code + "," + StudentName + "," + MidtermPoint + "," + EndPoint + "," + OtherPoint + "," + FinalGrade;
    }

}
