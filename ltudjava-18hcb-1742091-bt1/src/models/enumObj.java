/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author HoiNT2
 */
public enum enumObj {
    studentOb,
    classObj,
    accountObj,
    timetableObj,
    pointObj;

    public static Object getEnum(enumObj eObj, String[] arr) {
        Object obj = null;
        switch (eObj) {
            case studentOb:
                obj = new studentObj(arr);
                break;
            case classObj:
                obj = new classObj(arr);
                break;
            case accountObj:
                obj = new accountObj(Integer.parseInt(arr[0]), arr[1], arr[2], Integer.parseInt(arr[3]), arr[4]);
                break;
            case timetableObj:
                obj = new timetableObj(Integer.parseInt(arr[0]), arr[1], arr[2], arr[3]);
                break;
            case pointObj:
                obj = new pointObj(Integer.parseInt(arr[0]), arr[1], arr[2], Float.parseFloat(arr[3]), Float.parseFloat(arr[4]), Float.parseFloat(arr[5]), Float.parseFloat(arr[6]));
                break;
            default:
                break;
        }
        return obj;
    }
}
