/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import Helpers.FileHelper;
import Helpers.MessageHelper;
import Helpers.NumberHelper;
import common.constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import models.accountObj;
import models.enumObj;
import models.classObj;
import models.studentObj;

/**
 *
 * @author HoiNT2
 */
public class frmQLSinhVien extends javax.swing.JFrame {

    accountObj acc = null;
    private static String filePath = "data/17HCB.csv";
    private static String fileClassPath = "data/Class.csv";
    ArrayList<studentObj> lstSv = new ArrayList<>();
    ArrayList<classObj> lstClass = new ArrayList<>();

    /**
     * Creates new form frmQLSinhVien
     */
    public frmQLSinhVien() {
        initComponents();
        initClass();
        loadjTable(filePath);
    }

    public frmQLSinhVien(accountObj acc) {
        this.acc = acc;
        initComponents();
        initClass();
        hidePer(acc);
        loadjTable(filePath);
    }

    private void hidePer(accountObj acc) {
        if (acc != null && acc.getGroupId() == 1) {
            btn_import.hide();
            btn_save.hide();
        }
    }

    private void setTable(ArrayList<studentObj> lst) {
        try {
            if (lst == null) {
                JOptionPane.showMessageDialog(this, "Hệ thống bị lỗi không lấy đươc dữ liêu.");
                return;
            }
            DefaultTableModel tableModel = (DefaultTableModel) table_sv.getModel();
            tableModel.setRowCount(0);
            lst.stream().forEach(x -> {
                tableModel.addRow(new Object[]{x.getSTT(), x.getMSSV(), x.getHoTen(), x.getGioiTinh(), x.getCMND()});
            });
            table_sv.setModel(tableModel);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Hệ thống bị lỗi không lấy đươc dữ liêu.");
        }
    }

    private void loadjTable(String fileName) {
        try {
            lstSv = FileHelper.readFileNew(studentObj.class, fileName, enumObj.studentOb);
            if (lstSv != null && lstSv.size() > 0) {
                setTable(lstSv);
            } else {
                setTable(new ArrayList<>());
            }
            //TODO: Table sv
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void initClass() {
        try {
            lstClass = FileHelper.readFileNew(classObj.class, fileClassPath, enumObj.classObj);
            if (lstClass != null && lstClass.size() > 0) {
                lstClass.stream().forEach((itemGroup) -> {
                    jcbClass.addItem(itemGroup);
                });
            }
            //TODO: Table sv
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Them sv
     */
    private void submitForm() {
        String hoten = txt_hoten.getText().trim();
        String mssv = txt_masv.getText().trim();
        String gioiTinh = jcbGender.getSelectedItem().toString();
        String cmnd = txt_cmnd.getText().trim();
        String messgae = checkValidate(hoten, mssv, gioiTinh, cmnd);
        if (messgae.isEmpty()) {
            int stt = maxSTT();
            if (stt == -1 || stt > 1) {
                stt += 1;
                //TODO: thong bao loi khong co sinh vien
                lstSv.add(new studentObj(stt, mssv, hoten, gioiTinh, cmnd));
                try {
                    boolean result = FileHelper.writeFile(lstSv, filePath, constants.TITLE_STUDENTS);
                    if (result) {
                        MessageHelper.Message("Thêm sinh viên thành công.");
                    } else {
                        MessageHelper.Message("Lỗi hệ thống.Xin vui lòng thử lại sau");
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            } else {
                MessageHelper.Message("Lỗi hệ thống.Xin vui lòng thử lại sau");
            }
        } else {
            MessageHelper.Message(messgae);
        }
    }

    private String checkValidate(String hoten, String mssv, String gioiTinh, String cmnd) {
        if (hoten.isEmpty()) {
            return "ho ten khong duoc de trong";
        }
        if (!NumberHelper.isNumber(cmnd) || cmnd.length() < 9 || cmnd.length() > 11) {
            return "cmnd bat buoc la so va tu 10 den 11 so";
        }
        if (!NumberHelper.isNumber(mssv) || mssv.length() < 5) {
            return "mssv bat buoc la so va it nhat phai 5 so";
        }
        return validateSV(mssv, cmnd);
    }

    /**
     * get STT max
     *
     * @return
     */
    private int maxSTT() {
        if (lstSv == null || lstSv.size() == 0) {
            return -1;
        }
        return lstSv.stream().max(Comparator.comparing(v -> v.STT)).get().getSTT();
    }

    /**
     * Kiem tra sinh vien ton tai ma so sinh vien hoac cmnd
     *
     * @param massv
     * @param cmnd
     * @return
     */
    private String validateSV(String massv, String cmnd) {
        if (lstSv != null && lstSv.size() > 0) {
            for (studentObj item : lstSv) {
                if (item.MSSV.equalsIgnoreCase(massv)) {
                    return "MSSV nay da ton tai";
                }
                if (item.CMND.equalsIgnoreCase(cmnd)) {
                    return "CMND nay da ton tai";
                }
            }
        }
        return "";
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_masv = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_hoten = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_cmnd = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jcbClass = new javax.swing.JComboBox();
        btn_save = new javax.swing.JButton();
        jcbGender = new javax.swing.JComboBox<String>();
        btn_import = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_sv = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("QUẢN LÝ SINH VIÊN");
        setName(""); // NOI18N

        jLabel1.setText("MSSV");

        txt_masv.setName("txtMSSV"); // NOI18N

        jLabel2.setText("Họ Tên:");

        txt_hoten.setName("txtHoTen"); // NOI18N

        jLabel3.setText("Giới tính:");

        jLabel4.setText("CMND:");

        txt_cmnd.setName("txtCMND"); // NOI18N

        jLabel5.setText("Lớp:");

        jcbClass.setName("cbxLop"); // NOI18N
        jcbClass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbClassActionPerformed(evt);
            }
        });

        btn_save.setText("Thêm");
        btn_save.setName("btnThem"); // NOI18N
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        jcbGender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nam", "Nữ" }));

        btn_import.setText("Import file");
        btn_import.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_importActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(jcbClass, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txt_masv, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(txt_hoten, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(57, 57, 57)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_save, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_import, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2))
                    .addComponent(jcbGender, 0, 222, Short.MAX_VALUE)
                    .addComponent(txt_cmnd))
                .addGap(40, 40, 40))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txt_masv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_cmnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txt_hoten, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_import)
                    .addComponent(btn_save)
                    .addComponent(jcbClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        jLabel1.getAccessibleContext().setAccessibleName("MSSV:");
        jLabel2.getAccessibleContext().setAccessibleName("Họ tên:");

        table_sv.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "STT", "MSSV", "Họ tên", "Giới tính", "CMND"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(table_sv);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jcbClassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbClassActionPerformed
        // TODO add your handling code here:
        String text = jcbClass.getSelectedItem().toString();
        if (!text.equalsIgnoreCase("Chọn Lớp")) {
            filePath = "data/" + text + ".csv";
            loadjTable(filePath);
        }
    }//GEN-LAST:event_jcbClassActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        // TODO add your handling code here:
        submitForm();
        jcbClassActionPerformed(evt);
    }//GEN-LAST:event_btn_saveActionPerformed
    /**
     *
     * @param evt
     */
    private void btn_importActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_importActionPerformed
        // TODO add your handling code here:
        try {
            ArrayList<studentObj> lstStudentNew = FileHelper.readFileNew(studentObj.class, "", enumObj.studentOb);
            if (lstStudentNew != null && lstStudentNew.size() > 0) {
                int stt = maxSTT();
                if (stt == -1 || stt > 1) {
                    stt += 1;
                }
                
                if (lstSv == null) {
                    lstSv = new ArrayList<>();
                }
                if (MessageHelper.showConfirmDialog("Bạn có muốn ghi đè dữ liệu hay không")) {
                    if (!lstSv.isEmpty()) {
                        lstSv = new ArrayList<>();
                    }
                    // ghi đè dữ liệu giữ lại STT
                    for (studentObj item2 : lstStudentNew) {
                        ArrayList<studentObj> studentFlag = findByCodeStudent(item2.getMSSV());
                        if (studentFlag != null && studentFlag.size() > 0) {
                            for (studentObj item : studentFlag) {
                                item.setHoTen(item2.getHoTen());
                                item.setCMND(item2.getCMND());
                                item.setGioiTinh(item2.getGioiTinh());
                            }
                        } else {
                            studentObj stu = new studentObj(stt, item2.getMSSV(), item2.getHoTen(), item2.getGioiTinh(), item2.getCMND());
                            lstSv.add(stu);
                            stt += 1;
                        }
                    }
                } else {
                    // không ghi đè thì load lại STT
                    for (studentObj item2 : lstStudentNew) {
                        studentObj stu = new studentObj(stt, item2.getMSSV(), item2.getHoTen(), item2.getGioiTinh(), item2.getCMND());
                        lstSv.add(stu);
                        stt += 1;
                    }
                }
                try {
                    boolean result = FileHelper.writeFile(lstSv, filePath, "SST,MSSV,Họ tên,Giới tính,CMND");
                    if (result) {
                        setTable(lstSv);
                        MessageHelper.Message("Import dữ liệu thành công");

                    } else {
                        MessageHelper.Message("Lỗi hệ thống.Xin vui lòng thử lại sau");
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            //TODO: Table sv
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        jcbClassActionPerformed(evt);
    }//GEN-LAST:event_btn_importActionPerformed
    public ArrayList<studentObj> findByCodeStudent(String code) {
        ArrayList<studentObj> arr = new ArrayList<>();
        if (lstSv != null) {
            for (studentObj item : lstSv) {
                if (item.getMSSV().equals(code)) {
                    arr.add(item);
                }
            }
        }
        int size = arr.size();
        if (size > 0) {
            return arr;
        }
        return null;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmQLSinhVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmQLSinhVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmQLSinhVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmQLSinhVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmQLSinhVien().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_import;
    private javax.swing.JButton btn_save;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox jcbClass;
    private javax.swing.JComboBox<String> jcbGender;
    private javax.swing.JTable table_sv;
    private javax.swing.JTextField txt_cmnd;
    private javax.swing.JTextField txt_hoten;
    private javax.swing.JTextField txt_masv;
    // End of variables declaration//GEN-END:variables
}
